#!/bin/bash 

# Copy this script from livecd to installed base arch system 

echo "Install my-arch-customizations"

pacman -S sudo  git nano vim 

#pacman -S base-devel python python2 leafpad networkmanager network-manager-applet dhcp dhcpcd  wpa_supplicant 

pacman -S openssh bash  bash-completion  htop curl wget tmux  sakura terminator termite \
    linux-lts geany i3 i3blocks i3status i3lock neofetch dmenu scrot nitrogen conky udiskie volumeicon pulseaudio pulseaudio-alsa alsa-utils pasystray firefox \
    xorg xorg-xinit  xorg-drivers xorg-server xorg-twm ttf-dejavu xorg-fonts extra/font-bh-ttf xdg-user-dirs-gtk virtualbox-guest-utils xterm picom \
    plasma-workspace-wallpapers epdfview qpdfview qtox gpicview-gtk3 scrot rhythmbox audacious audacity mpv vlc gstreamer snappy-player \
    mousepad xarchiver ristretto xfburn thunar galculator libreoffice-fresh seamonkey calibre gparted clamav rsync

if [[ -d my-arch ]]; then 
   cd my-arch 
   git pull --rebase
   cd ..
else 
   git clone https://jacek_kowalczyk@bitbucket.org/jacek_kowalczyk/my-arch.git    
fi 

mkdir -p /opt ||true 
rsync -avz my-arch/livecd/airootfs/etc/skel /etc/
rsync -avz my-arch/livecd/airootfs/opt/dwm /opt/

rsync -avz my-arch/livecd/airootfs/usr/local/bin/startgui /usr/local/bin/

rsync -avz my-arch/livecd/airootfs/usr/bin/dwm /usr/bin/
rsync -avz my-arch/livecd/airootfs/usr/bin/st /usr/bin/


read -p "Please enter your new user name: " choice
useradd -d /home/$choice -m -s /bin/bash $choice
passwd $choice
usermod -aG wheel $choice

echo "Rebooting , "
echo "  After reboot login with new user: $choice, and run startgui command"
echo "Remember to enable wheel group in /etc/sudoers"

read -p "Please hit any key and enter: " anykey
reboot 



