# README 

How to setup and customize arch linux?

![my-arch-dwm-gaps.png](./my-arch-dwm-gaps.png)

### What is this repository for? ###

* To document my setup and customization of arch linux 
* https://wiki.archlinux.org/index.php/Installation_guide
* 

### Links 

* nice arch installer - https://sourceforge.net/projects/archfi/ 
* https://sourceforge.net/projects/archdi/
* https://github.com/MatMoul/archdi
* https://github.com/MatMoul/archfi 
* https://www.youtube.com/playlist?list=PLytHgIKLV1caHlCrcTSkm5OF2WSVI1_Sq

* Chris Titus Tech: How to Install Arch Linux with Desktop Environment | Archfi - https://www.youtube.com/watch?v=GUtpHFI-vjg

## Install by my-arch custom ISO

* Get the ISO from https://sourceforge.net/projects/my-arch/files/ 
* Boot my-arch iso 

```
bash ./get-archfi.sh
# install Arch using Archfi installer
# Password for livecd user is livecd
sudo bash ./archfi 

#when the new file system created by installer is mounted  copy the install-my-customizations.sh to new file system to /opt/ dir
# new root file system is mounted to /mnt

sudo cp /opt/install-my-customizations.sh /mnt/opt

```

* Once the installation by Archfi installer is finished, reboot conmputer, login as root and start  /opt/install-my-customizations.sh script 

```
bash  /opt/install-my-customizations.sh
```


## Older notes regarding setup from regular arch installer and building custom ISO

### How do I get set up? ###

* base system setup 

```
mkfs.ext4 /dev/sda1
mkfs.ext4 /dev/sda2
mkswap /dev/sda3
swapon /dev/sda3

mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

pacstrap /mnt base base-devel xorg i3 xorg-drivers xorg-fonts

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
hwclock --systohc
date


nano /etc/locale.gen


locale-gen
nano /etc/locale.conf and enable en_US.UTF-8 UTF-8
LANG=en_US.UTF-8
LC_ALL=C


nano /etc/vconsole.conf
KEYMAP=pl

nano /etc/hostname
myhostname


nano /etc/hosts
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname

pacman -S iw 
pacman -S wpa_supplicant

passwd

pacman -S intel-ucode grub
grub-install --target=i386-pc /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg


#pacman -S networkmanager
#pacman -S network-manager-applet
# or

pacman -S dhcp dhcpcd 
systemctl enable dhcpcd.service

exit 

reboot 

```

* Configuring network 

```
systemctl enable NetworkManager.service
systemctl start NetworkManager.service

```
* use `nmtui` to activate network connections


### Create standard user 

```
pacman -S sudo 
useradd -d /home/kowalczy -m -s /bin/bash kowalczy 
#useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/bash kowalczy

passwd kowalczy

usermod -aG wheel,storage,power,adm,audio,network kowalczy 


```

### Adding AUR helper yay 

```
pacman -S git
git clone https://aur.archlinux.org/yay.git
cd yay 
makepkg -si
```

### Configuring X systems

```

pacman -S i3 i3blocks dmenu i3status i3lock screenfetch neofetch

yay dwm 
yay st-git

pacman -S xorg xorg-xinit 
pacman -S xorg-drivers xorg-fonts

pacman -S vim 
pacman -S leafpad

pacman -S virtualbox-guest-utils 

``` 

* Setup i3 to startup 

```
cp /etc/X11/xinit/xinitrc ~/.xinitrc
```
* nano ~/.xinitrc  , comment default startup lines and set 

```
exec i3 
```

* vim ~/.config/i3/config and append lines:

```
#my autostart 

exec --no-startup-id nitrogen --restore & 
exec --no-startup-id nm-applet & 
exec --no-startup-id udiskie -ant --use-udisks2 & 
exec --no-startup-id volumeicon & 


bindsym $mod+Shift+p exec "i3-nagbar -t warning -m 'You pressed the system poweroff shortcut. Do you really want to shut the system down ?' -b 'Yes, PowerOff now' 'systemctl poweroff' "
bindsym $mod+Shift+b exec "i3-nagbar -t warning -m 'You pressed the system reboot shortcut. Do you really want to reboot the system ?' -b 'Yes, Reboot now' 'systemctl reboot' "

```
* configure xterm 

```


nano ~/.Xdefaults:

xterm*background: black
xterm*foreground: white 

#For real transparency (composite):
urxvt*depth: 32
urxvt*background: rgba:0000/0000/0200/c800

# For fake transparency:
urxvt*transparent: true
urxvt*shading: 30



```

* startx

* install additional customization packages

```
pacman -S nitrogen archlinux-wallpaper plasma-workspace-wallpapers

```

* install dev and utils packages

```
pacman -S git meld openssh htop mc sudo ranger rox gufw 
pacman -S pulseaudio volumeicon udiskie 

```

### Create standard user 

```
pacman -S sudo 
useradd -d /home/kowalczy -m -s /bin/bash kowalczy 

useradd -m -p $(perl -e 'print crypt("livecd","this-is-random-text")') -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/bash livecd | true


passwd kowalczy

usermod -aG adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel kowalczy 

```

### yaourt - **DEPRECATED**

```
sudo pacman -S --neede base-devel

```
* edit file /etc/pacman.conf and uncomment multilib secion 
* edit file /etc/pacman.conf and add 

```
[archlinuxfr]
SigLevel = Never
Server = http://repo.archlinux.fr/$arch

```

* run `sudo pacman -Syu yaourt` 

## Archiso create livecd iso

* Install packages 

```
sudo pacman -S --needed base-devel 
sudo pacman -S make patch 
yay archiso-git 
sudo pacman -S coreutils mlocate


sudo updatedb 
locate archiso |less 

```

* edit file /etc/pacman.conf and uncomment multilib secion 
* edit file /etc/pacman.conf and add 

```

[archlinuxpl]
SigLevel = Optional 
Server = http://ftp.vectranet.pl/archlinux/


```

* **OPTIONALLY** edit file /usr/bin/mkarchiso and add option -i to pacstrap commands in two places 

```
...
pacstrap -C "${pacman_conf}" -i -c -d -G -M "${work_dir}/airootfs" $* &> /dev/null
...
pacstrap -C "${pacman_conf}" -i -c -d -G -M "${work_dir}/airootfs" $*
...

```

* Get archiso config template

```
mkdir -p ~/my-arch/livecd/out

sudo cp -r /usr/share/archiso/configs/releng/* ~/my-arch/livecd/
cd ~/my-arch/livecd

```



### Edit these files  and do required changes

* packages.x86_64  - add new pckages to be installed

```
base-devel
python
python2
leafpad
networkmanager
network-manager-applet
dhcp
dhcpcd 
wpa_supplicant
openssh
bash 
bash-completion
sudo
git
htop
curl
vim
nano
wget
tmux 
termite
terminator
geany

i3
i3blocks
i3status
i3lock
neofetch
dmenu
scrot


nitrogen
conky
udiskie
volumeicon
pulseaudio
pulseaudio-alsa
alsa-utils
pasystray


chromium
falkon
firefox-developer-edition
firefox
midori


xorg
xorg-xinit 
xorg-drivers
xorg-server
xorg-twm
ttf-dejavu
xorg-fonts
extra/font-bh-ttf
xdg-user-dirs-gtk
virtualbox-guest-utils
xterm
compton

plasma-workspace-wallpapers
epdfview
qpdfview
qtox
gpicview-gtk3

scrot 
rhythmbox
audacious
audacity
mpv 
vlc
gstreamer
snappy-player

thunar
libreoffice-fresh 
seamonkey 
calibre


```
* mkinitcpio.conf

```
cp mkinitcpio.conf backup_mkinitcpio.conf

leafpad mkinitcpio.conf  # remove PXE kernel modules 
#remove archiso_pxe_common archiso_pxe_nbd archiso_pxe_http archiso_pxe_nfs 


```

* build.sh

```

sudo leafpad build.sh    # remove PXE kernel modules
#remove archiso_pxe_common archiso_pxe_nbd archiso_pxe_http archiso_pxe_nfs 

```

* airootfs/root/customize_airootfs.sh 

```
sudo leafpad airootfs/root/customize_airootfs.sh  # customize  the new ISO image 

# content to be added 

echo "Adding livecd user"
#useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/bash livecd
useradd -m -p $(perl -e 'print crypt("livecd","this-is-random-text")') -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /usr/bin/bash livecd | true
echo "livecd user - DONE"

systemctl enable dhcpcd.service
systemctl enable NetworkManager.service
systemctl start NetworkManager.service
echo "systemctl changes - DONE"

echo "cp -r /etc/skel/* /root/"
cp -r /etc/skel/.* /root/ | true 
cp -r /etc/skel/xinitrc* /root/ | true 
echo "cp -r /etc/skel/* /root/ - DONE"

echo "Enable sudo"
echo "%wheel ALL=(ALL) ALL " >> /etc/sudoers  | true
echo "Enable sudo - DONE"

echo "Update root password "
echo -e "toor\ntoor" | passwd root | true 
echo "Update root password - DONE"

echo "Custmize script Finished"

```

* airootfs/etc/pacman.conf

```
cd airootfs/etc/    
sudo cp /etc/pacman.conf pacman.conf
sudo leafpad pacman.conf

```
 
### Build ISO 
 
```
sudo rm -rf work 
date && sudo ./build.sh -v | tee -a ~/archiso-build-`date '+%Y-%m-%d'`.log

```

### Manual hacks and customization 

```
sudo mount --bind ~/my-arch/livecd/work/x86_64/airootfs/ ~/my-arch/livecd/work/x86_64/airootfs/
sudo arch-chroot ~/my-arch/livecd/work/x86_64/airootfs

#do all hacks 

sudo umount /home/kowalczy/my-arch/livecd/work/x86_64/airootfs
sudo ./build.sh -v 


```


## custom archiso  other manuals 

* part1 https://www.youtube.com/watch?v=y_Blo7hB8Ag
* part2 https://www.youtube.com/watch?v=uQxHEcUgKLw
  * https://paste.ubuntu.com/8093544/
  * https://paste.ubuntu.com/8093552/

* other  https://www.youtube.com/watch?v=DqV1BJtJXEA

* [Easiest way to install Arch Linux in 2018 with Xorg and a Desktop](https://www.youtube.com/watch?v=6c3aZabI8uU)
  * https://paste.ubuntu.com/23956628/

```
Before installing Arch Linux, make sure your computer is connected to the internet.

# dhcpcd
# ping -c 3 archlinux.org

If none is available, stop the dhcpcd service with systemctl stop dhcpcd@<TAB> and see Network configuration. https://wiki.archlinux.org/index.php/Network_configuration#Device_driver

Partitioning Disk

|--------------------------------------------------------------------|
| HDD 8GB - RAM 3GB |
|--------------------------------------------------------------------|
| /DEV/SDA | SIZE | MOUNT POINT | 
|--------------------------------------------------------------------|
| sda1 | 10GB | / |
|--------------------------------------------------------------------|
| sda2 | 1GB | swap |
|--------------------------------------------------------------------|
| sda3 | 9GB | /home |
|--------------------------------------------------------------------|

# lsblk
# cfdisk /dev/sda

Format the partition sda1

# mkfs.ext4 /dev/sda1

Activate the swap partition

# mkswap /dev/sda2
# swapon /dev/sda2

Format the partition sda3

# mkfs.ext4 /dev/sda3

Mount the file systems

# mount /dev/sda1 /mnt

# mkdir /mnt/home
# mount /dev/sda3 /mnt/home

Choose closest mirror list

# pacman -Sy
# pacman -S reflector
# reflector --verbose -l 5 --sort rate --save /etc/pacman.d/mirrorlist

Install arch linux base packages

# pacstrap -i /mnt base base-devel

Configure Fstab and chroot /mnt

# genfstab -U -p /mnt >> /mnt/etc/fstab
# cat /mnt/etc/fstab
# arch-chroot /mnt

Configure language and location

# nano /etc/locale.gen
uncomment: en_US.UTF-8 UTF8 
# locale-gen
# echo LANG=en_US.UTF-8 > /etc/locale.conf
# export LANG=en_US.UTF-8

Set time zone

# ls /usr/share/zoneinfo
# ln -s /usr/share/zoneinfo/America/New_York > /etc/localtime
ln -s /usr/share/zoneinfo/Region/City /etc/localtime

# hwclock --systohc --utc

Configure the repository

# nano /etc/pacman.conf
Uncomment the line: [multilib] and include = /etc/pacman.d/mirrorlist

# pacman -Sy

Set hostname and network

# echo linux-fanboy > /etc/hostname
# systemctl enable dhcpcd@enp0s3.service

Set root password and create new user

# passwd
# pacman -S sudo bash-completion
# useradd -m -g users -G wheel,storage,power -s /bin/bash henri
# passwd henri

Allow the users in wheel group to be able to preformance administrative tasks with sudo:

# EDITOR=nano visudo
Uncomment the line: %wheel ALL=(ALL)

Install and configure bootloader

# mkinitcpio -p linux
# pacman -S grub os-prober
# grub-install /dev/sda
# grub-mkconfig -o /boot/grub/grub.cfg

Unmount the partitions and reboot

# exit
# umount -R /mnt
# reboot
```

* https://www.youtube.com/watch?v=4PBqpX0_UOc


## Other manuals 

### Gnome arch manual 

* https://itsfoss.com/install-arch-linux/

## cli install 

* https://www.ostechnix.com/install-arch-linux-latest-version/
* https://www.wikihow.com/Install-Arch-Linux
* http://www.linuxandubuntu.com/home/a-step-by-step-guide-to-install-arch-linux

## Revenge installer

* https://www.ostechnix.com/installing-arch-linux-using-revenge-graphical-installer/


## Install Arch iso with AUI 

* different approach, setup arch with AUI arch linux installer 
  * https://www.youtube.com/watch?v=yuX8eijwZ9A
  * https://github.com/helmuthdu/aui

```

wget https://github.com/helmuthdu/aui/tarball/master -O - | tar xz

```
